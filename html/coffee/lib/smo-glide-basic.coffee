$(window).load ->
		$('.slide-background-image').each (index,value) ->
			$(value).closest('article').css('background-image', "url("+$(value).find('img').attr('src')+")")
			$(value).find('img').hide()

$(document).ready ->
	$('.slider.basic').glide
		arrowRightText: '<i class="fa fa-angle-right"></i>',
		arrowLeftText: '<i class="fa fa-angle-left"></i>'
