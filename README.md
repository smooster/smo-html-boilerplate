# smooster html boilerplate for fast project kickoff (smo-html-boilerplate) Version 0.1.0

the html boilerplate we use to kickoff our new html projects

## Installation

    $ git clone git@bitbucket.org:smooster/smo-html-boilerplate.git

  or just download it via [Downloads](https://bitbucket.org/smooster/smo-html-boilerplate/downloads)

## Usage

  Use the html/index.html as your first template file, if needed you can add more html files in /html. 


## Wishlist
* None so far

## Changelog

###0.1.0
* Basic folder and file structure
* Some first examples like slideshow & navigation in the boilerplate file

## Contributing

1. Fork it ( https://bitbucket.org/smooster/smo-html-boilerplate/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request